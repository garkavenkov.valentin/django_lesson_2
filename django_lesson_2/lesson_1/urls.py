from django.urls import path, re_path
from . import views

urlpatterns = [
    path('index', views.index, name='index-view'),
    re_path('bio/(?P<title>[\w]+)/$', views.bio, name='bio'),
]
