from django.shortcuts import render
from django.http import HttpRequest, HttpResponse

# Create your views here.
def base(request: HttpRequest) -> HttpRequest:
    return HttpResponse("Basic page")

def lesson2(request: HttpRequest) -> HttpRequest:
    return HttpResponse("Lesson_2")

def start(request: HttpRequest) -> HttpResponse:
    return HttpResponse('Start')