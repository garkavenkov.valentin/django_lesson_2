from django.urls import path, re_path
from . import views

urlpatterns = [
    path('', views.base),
    path('start/', views.start),
]