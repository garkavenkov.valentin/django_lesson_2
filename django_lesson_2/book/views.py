from django.shortcuts import render
from django.http import HttpRequest, HttpResponse

# Create your views here.
def book(request: HttpRequest) -> HttpResponse:
    return HttpResponse(f'localhost/book/')

def book_2(request: HttpRequest, title) -> HttpResponse:
    return HttpResponse(f'localhost/book/{title}')