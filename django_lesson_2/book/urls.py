from django.urls import path, re_path
from . import views

urlpatterns = [
    path('', views.book),
    re_path('(?P<title>[\w]+)/$', views.book_2),
]